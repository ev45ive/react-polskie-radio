import axios, { AxiosError } from "axios";

axios.interceptors.request.use(config => {
  config.headers['Authorization'] = 'Bearer ' + AuthService.token
  return config
}, err => err);

axios.interceptors.response.use(config => config, (err: AxiosError) => {
  if (err.response?.data?.error) {
    if (err.response.status === 401) {
      setTimeout(() => AuthService.authorize(), 1000)
    }
    return Promise.reject(err.response?.data.error)
  }
  console.error(err)
  const error = new Error('Unexpected error!');
  (error as any).originalError = err;
  return Promise.reject()
})


export const AuthService = {
  token: '' as string | null,

  init() {
    this.token = window.sessionStorage.getItem('token');
    if (this.token) { return }

    const parms = new URLSearchParams(window.location.hash);
    const token = parms.get('#access_token');
    if (token) {
      this.token = token;
      window.sessionStorage.setItem('token', token);
      window.location.hash = ''
    }

    if (!this.token) {
      this.authorize();
    }
  },

  authorize() {
    window.sessionStorage.removeItem('token');
    const params = new URLSearchParams({
      client_id: 'bceba94c95024f3080c7d8b8a4278f1b',
      response_type: 'token',
      redirect_uri: 'http://localhost:3000/',
      show_dialog: 'true'
    });

    const url = `https://accounts.spotify.com/authorize?${params}`;
    window.location.replace(url);
  }
};
