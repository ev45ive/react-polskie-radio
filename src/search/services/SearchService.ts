import { Album, AlbumsSearchResponse } from "../../core/models/Album"
import axios, { AxiosError, CancelTokenSource } from 'axios'
import { AuthService } from "./AuthService"

const mockAlbumsData = ([
  { id: '123', name: 'Album 123', images: [{ url: 'https://www.placecage.com/gif/300/300' }] },
  { id: '345', name: 'Album 345', images: [{ url: 'https://www.placecage.com/gif/400/400' }] },
  { id: '345', name: 'Album 345', images: [{ url: 'https://www.placecage.com/gif/500/500' }] },
] as Album[])

export const SearchService = {

  searchAlbums: (query: string) => {
    const source = axios.CancelToken.source();

    const promise = Promise.resolve().then(async () => {
      const resp = await axios.get<AlbumsSearchResponse>(//
        `https://api.spotify.com/v1/search`, {
        params: {
          type: 'album',
          q: query
        },
        cancelToken: source.token
      })
      return resp.data.albums.items
    }) as Promise<Album[]> & {
      source: CancelTokenSource
    }

    promise.source = source

    return promise;
  }

  // searchAlbums: (query: string) => {
  //   return axios.get<AlbumsSearchResponse>(//
  //     `https://api.spotify.com/v1/search`, {
  //     params: {
  //       type: 'album',
  //       q: query
  //     }
  //   }).then(resp => resp.data.albums.items)
  //   .catch(...)
  //   .finally(...)
  // }

}

