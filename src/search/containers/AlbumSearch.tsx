import React, { ReducerAction, useEffect, useReducer, useState } from 'react'
import { RouteComponentProps, useHistory, useLocation, useParams } from 'react-router-dom'
import { Loading } from '../../core/components/Loading'
import { Message } from '../../core/components/Message'
import { Album } from '../../core/models/Album'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import { SearchService } from '../services/SearchService'
import { reducer, initialState, SearchStart, SearchSuccess, SearchFailed } from './AlbumSearchReducer'

/* ================ */
interface Props extends RouteComponentProps { }

export const AlbumSearch = (props: Props) => {
  // const [state, dispatch] = useReducer(reducer, initialState)
  const [{ message, isLoading, results }, dispatch] = useReducer(reducer, initialState)
  const history = useHistory()
  // useParams()
  const location = useLocation()
  const queryParams = new URLSearchParams(location.search)
  const q = queryParams.get('q')

  const searchAlbums = async (query: string) => {

  }

  const changeQuery = (query: string) => {
    history.push('/search?q=' + query)
  }

  useEffect(() => {
    if (!q) { return }
    let source: any

    (async () => {
      try {
        dispatch(SearchStart(q))
        const promise = SearchService.searchAlbums(q)
        source = promise.source
        dispatch(SearchSuccess(await promise))
      } catch (error) {
        if (error.message !== 'cancel')
          dispatch(SearchFailed(error))
      }
    })()
    return () => source.cancel('cancel')
  }, [q])

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={changeQuery} query={q || ''} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Loading loading={isLoading}>
            <Message className="alert-danger">{message}</Message>
            <SearchResults results={results} />
          </Loading>
        </div>
      </div>
    </div>
  )
}
