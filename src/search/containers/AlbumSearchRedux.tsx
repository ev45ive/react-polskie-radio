import React, { ReducerAction, useEffect, useReducer, useState } from 'react'
import { useDispatch, useSelector, useStore } from 'react-redux'
import { RouteComponentProps, useHistory, useLocation, useParams } from 'react-router-dom'
import { Loading } from '../../core/components/Loading'
import { Message } from '../../core/components/Message'
import { Album } from '../../core/models/Album'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import { SearchService } from '../services/SearchService'
import { SearchStart, SearchSuccess, SearchFailed, getSearchResults, getSearchMeta, searchAlbums } from '../../reducers/search.reducer'

/* ================ */
interface Props extends RouteComponentProps { }

export const AlbumSearchRedux = (props: Props) => {
  // const { dispatch, getState } = useStore()
  const results = useSelector(getSearchResults)
  const { loading: isLoading, message, query } = useSelector(getSearchMeta)
  const dispatch = useDispatch()


  const history = useHistory()
  // useParams()
  const location = useLocation()
  const queryParams = new URLSearchParams(location.search)
  const q = queryParams.get('q')

  const changeQuery = (query: string) => {
    history.push('/search?q=' + query)
  }

  useEffect(() => {
    if (!q) { return }

    dispatch(searchAlbums(q))
  }, [q])

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={changeQuery} query={q || query} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Loading loading={isLoading}>
            <Message className="alert-danger">{message}</Message>
            <SearchResults results={results} />
          </Loading>
        </div>
      </div>
    </div>
  )
}
