import { act, render, screen, waitFor } from '@testing-library/react'
import React from 'react'
import { AlbumSearch } from './AlbumSearch'
import { SearchResults } from '../components/SearchResults'
import { SearchForm } from '../components/SearchForm'
import { SearchService } from '../services/SearchService'

jest.mock('../services/SearchService')
jest.mock('../components/SearchResults')
jest.mock('../components/SearchForm')

describe.only('AlbumSearch', () => {
  let SearchFormMOCK: jest.Mock
  let SearchResultsMock: jest.Mock

  beforeEach(() => {
    SearchFormMOCK = (SearchForm as jest.Mock).mockReturnValue('SearchFormMOCK')
    SearchResultsMock = (SearchResults as jest.Mock).mockReturnValue('SearchResultsMOCK');

    // (SearchService.searchAlbums as jest.Mock).mockResolvedValue([]);
    const { rerender } = render(<AlbumSearch />)
  })

  it('shows empty search results', () => {
    expect(screen.getByText('SearchResultsMOCK')).toBeVisible()
  })

  it('shows empty search form', () => {
    expect(screen.getByText('SearchFormMOCK')).toBeVisible()
  })

  it('make search requests from form', async () => {
    const { onSearch } = SearchFormMOCK.mock.calls[0][0];
    const resultPromise = Promise.resolve([]);
    (SearchService.searchAlbums as jest.Mock).mockReturnValue(resultPromise);
    
    await act(async () => {
      // FireEvent.click(getByRole('button'))
      onSearch('placki');
      expect(SearchService.searchAlbums as jest.Mock).toHaveBeenCalledWith('placki')
      await resultPromise
    });
  })

  it('renders search request results', async () => {
    const { onSearch } = SearchFormMOCK.mock.calls[0][0];
    const resultPromise = Promise.resolve([
      { name: 'placki' }
    ]);
    (SearchService.searchAlbums as jest.Mock).mockReturnValue(resultPromise);

    await act(async () => {
      onSearch('placki')
      await resultPromise
    });
    // await waitFor(() => screen.getByTestId('response-item'))
    // expect(screen.getByTestId('response-item')).toHaveLength(1)
    const { results } = SearchResultsMock.mock.calls.pop()[0]
    expect(results).toEqual([{ name: 'placki' }])
  })


})
