import React, { useEffect, useState } from 'react'

interface Props {
  onSearch(query: string): void
  query: string
}

export const SearchForm = (props: Props) => {
  const [query, setQuery] = useState(props.query)

  useEffect(() => {
    setQuery(props.query)
  }, [props.query])

  useEffect(() => {
    const handler = setTimeout(() => {
      props.onSearch(query)
    }, 500)

    return () => clearTimeout(handler)
  }, [query])

  return (
    <div>
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Search"
          value={query}
          onChange={e => setQuery(e.target.value)}
          onKeyUp={(e) => e.key === 'Enter' && props.onSearch(query)} />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" type="button" onClick={() => props.onSearch(query)}>
            Search
          </button>
        </div>
      </div>

    </div>
  )
}
