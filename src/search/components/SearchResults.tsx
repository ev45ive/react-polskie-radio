import React from 'react'
import { Album } from '../../core/models/Album'

interface Props {
  results: Album[]
}

export const SearchResults = (props: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 no-gutters">
        {props.results.map(result => <div className="col mb-4" key={result.id}>
          <div className="card">
            <img src={result.images[0].url} className="card-img-top" />

            <div className="card-body">
              <h5 className="card-title">{result.name}</h5>
            </div>
          </div>
        </div>)}
      </div>
    </div>
  )
}
