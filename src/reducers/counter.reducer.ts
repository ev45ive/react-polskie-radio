
export function counterReducer(state = 0, action: any) {
  switch (action.type) {
    case 'INC': return state + action.payload;
    case 'DEC': return state - action.payload;
    default: return state;
  }
}