import { Reducer } from 'react';
import { Dispatch } from 'redux';
import { Album } from '../core/models/Album';
import { SearchService } from '../search/services/SearchService';

interface State {
  query: string;
  results: Album[];
  message: string;
  isLoading: boolean;
}

interface SEARCH_START { type: 'SEARCH_START'; payload: { query: string; }; }
interface SEARCH_SUCCESS { type: 'SEARCH_SUCCESS'; payload: { results: Album[]; }; }
interface SEARCH_FAILED { type: 'SEARCH_FAILED'; payload: { error: Error; }; }
type ACTIONS = SEARCH_START |
  SEARCH_SUCCESS |
  SEARCH_FAILED;

export const initialState: State = {
  query: '',
  results: [],
  message: '',
  isLoading: false
};

export const searchReducer: Reducer<State, ACTIONS> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'SEARCH_START': return {
      ...state, results: [], message: '', isLoading: true, query: action.payload.query
    };
    case 'SEARCH_SUCCESS': return {
      ...state, isLoading: false, results: action.payload.results
    };
    case 'SEARCH_FAILED': return {
      ...state, isLoading: false, message: action.payload.error?.message
    };
    default:
      return state;
  }
};

/* Action Creators */
export const SearchStart = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } });

export const SearchSuccess = (results: Album[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { results } });

export const SearchFailed = (error: Error): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { error } });

/* Async (thunk) action craetors */

export const searchAlbums = (query: string) => async (dispatch: Dispatch) => {
  try {
    dispatch(SearchStart(query))
    const resp = await SearchService.searchAlbums(query)
    dispatch(SearchSuccess(resp))
  } catch (error) {
    dispatch(SearchFailed(error))
  }
}

/* Selectors */
export const getSearchResults = (store: { search: State }) => store.search.results

export const getSearchMeta = (store: { search: State }) => ({
  loading: store.search.isLoading,
  query: store.search.query,
  message: store.search.message,
})
