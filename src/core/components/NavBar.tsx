import React, { useState } from 'react'
import { NavLink, Route } from 'react-router-dom'
import { UserContext, UserContextProvider } from '../contexts/UserContext'
import { UserWidget } from './UserWidget'

interface Props {

}

export const NavBar = (props: Props) => {
  const [open, setOpen] = useState(false)
  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">

          {/* <a className="navbar-brand" href="#/">Navbar</a> */}
          <NavLink className="navbar-brand" to="/" exact={true}>Music App</NavLink>

          <button className="navbar-toggler" type="button" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" onClick={() => setOpen(!open)}>
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className={`collapse navbar-collapse  ${open ? 'show' : ''}`}>
            <ul className="navbar-nav">

              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="placki active" to="/search">Search</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>

              <Route path="/playlists">

                <li className="nav-item">
                  <NavLink className="nav-link" to="/playlists">Create New </NavLink>
                </li>
              </Route>
            </ul>
            
            <div className="ml-auto text-white navbar-text">
              <UserWidget />
            </div>

            {/* <div className="ml-auto text-white navbar-text">
              <UserContext.Consumer>{
                ({ login, logout, user }) => <>
                  {!user &&
                    <span>Welcome Anonym, <span onClick={login}>LogIn</span></span>}
                    
                  {user &&
                    <span>Welcome {user.display_name}, <span onClick={logout}>LogOut</span></span>}
                </>
              }</UserContext.Consumer>

            </div> */}

          </div>
        </div>
      </nav>
    </div>
  )
}
