import React, { FC } from 'react'

interface Props {
  loading: boolean
}

export const Loading: FC<Props> = ({ loading, children }) => {
  return (
    <>
      {loading && <div className="d-flex justify-content-center m-5">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>}

      {!loading && children}
    </>
  )
}
