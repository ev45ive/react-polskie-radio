import React, { FC } from 'react'

interface Props extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {

}

export const Message: FC<Props> = ({
  children: message,
  className,
  ...otherProps
}) => {
  return (
    <>
      {message && <p
        className={"alert " + className}
        {...otherProps}>
        {message}
      </p>}
    </>
  )
}
