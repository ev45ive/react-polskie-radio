import React, { useContext, useMemo } from 'react'
import { UserContext } from '../contexts/UserContext'

interface Props {

}

export const UserWidget = (props: Props) => {
  const { login, logout, user } = useContext(UserContext)


  return useMemo(() => (
    <div>
      {!user &&
        <span>Welcome Anonym, <span onClick={login}>LogIn</span></span>}

      {user &&
        <span>Welcome {user.display_name}, <span onClick={logout}>LogOut</span></span>}

    </div>
  ), [user])
}
