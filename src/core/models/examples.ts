export {}

// export interface Playlist {
//   id: number
//   name: string
//   public: boolean
//   description: string
// }

// import { Album } from "./Album"
// import { Playlist } from "./Playlist"

// import { Track } from "./Album"

// TYPE MAPPINGS:
// type PlaylistDraft = Partial<Playlist>
// export type PlaylistEditDraft = Pick<Playlist, 'id' | 'name' | 'public' | 'description'>

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }

// type DraftKeys = 'id' | 'name' | 'public'
// type PlaylistDraft = {
//   [k in DraftKeys]: Playlist[k]
// }
// type StringsRecord = Record<DraftKeys,string>

// type PlaylistDraft = {
//   id: Playlist['id']
//   name: Playlist['name']
//   public: Playlist['public']
//   description: Playlist['description']
// }


// let zmienna = 123
// zmienna = parseInt('123')
// let zmienna:'stala1' | 'stala2' = 'stala1'
// zmienna = 'stala'+'1'

// interface PagingObject<T>{
//   total:number;
//   items: Array<T>
// }
// type AlbumsPagination = PagingObject<Album>

// export interface Entity {
//   id: string
//   name: string
// }

// export interface Track extends Entity {
//   type: 'track'
//   duration_ms: number;
// }

// export interface Playlist {
//   id: string
//   name: string
//   type: 'playlist'
//   /**
//    * Is playlist publicly visible
//    */
//   public: boolean
//   description: string,
//   tracks?: Track[]
// }

// const X: Track | Playlist = {} as any

// if (X.type === 'playlist') {
//   X.tracks?.length
// } else {
//   X.duration_ms
// }

// function renderResult(result: Track | Playlist) {
//   if (result.type === 'playlist') {
//     return result.tracks
//   }
//   result.duration_ms
// }

// switch (X.type) {
//   case 'playlist':
//     X.tracks
//     break;
//   // case 'track':
//   default:
//     X.duration_ms
// }

// class PlaylistImpl implements Playlist {
//   public = false
//   constructor(
//     public id: number,
//     public name: string,
//     ispublic: boolean,
//     public description: string) {
//     this.public = ispublic
//   }
// }

// const p = new PlaylistImpl()
// p.name // undefined


// export interface Playlist {
//   id: number | string
// if('string' === typeof p.id){
//   p.id.toLocaleUpperCase()
// }else{
//   p.id.toExponential()
// }

// const p: Playlist = {
//   id: '123',
//   description: '',
//   name: '',
//   public: true,
// }

// if (p.tracks) {
//   p.tracks.items.length
// }

// p.tracks? p.tracks.items.length : undefined

// p.tracks && p.tracks.items.length

// const x = p.tracks?.items.length

// const y = p.tracks!.items.length

// Structural Typing
// // interface Vector { x: number; y: number }
// interface Vector { x: number; y: number, length: number }
// interface Point { x: number; y: number }

// let v: Vector = { x: 123, y: 123, length: 234 }
// let p: Point = { x: 123, y: 123 }

// p = v;
// // v = p; // Missing length
