import React, { useEffect, useLayoutEffect } from 'react';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import { AlbumSearch } from './search/containers/AlbumSearch';
import { AuthService } from "./search/services/AuthService";
// import logo from './logo.svg';
// import './App.css';
// import 'bootstrap/dist/css/bootstrap.css';
import { Redirect, Route, Switch } from 'react-router-dom'
import { NavBar } from './core/components/NavBar';
import { AlbumSearchRedux } from './search/containers/AlbumSearchRedux';

function App() {
  return (
    <div className="App">
      <NavBar />
      {/* .container>.row>.col>h2{Music App} */}

      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />
              <Route path="/playlists" component={PlaylistsView} />
              {/* <Route path="/search" component={AlbumSearch} /> */}
              <Route path="/search" component={AlbumSearchRedux} />
              <Route path="*" render={() => <h1>Page not found</h1>} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
