import { AnyAction, applyMiddleware, combineReducers, createStore, Dispatch, Middleware, MiddlewareAPI } from "redux";
import { counterReducer } from "./reducers/counter.reducer";
import { searchReducer } from "./reducers/search.reducer";

import thunkMiddleware from 'redux-thunk'


export const reducer = combineReducers({
  counter: counterReducer,
  search: searchReducer
})

// const thunkMiddleware: Middleware = (api) => (next) => (action: any) => {
//   // api.getState()
//   if (typeof action === 'function') {
//     action(next)
//     return
//   }

//   console.log(action)
//   const state = next(action)
//   console.log(state)
// };

export const store = createStore(reducer, applyMiddleware(
  thunkMiddleware
))


  ; (window as any).store = store