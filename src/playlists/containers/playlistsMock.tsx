import { Playlist } from "../../core/models/Playlist";


export const playlistsMock: Partial<Playlist>[] = [{
  id: '123',
  name: 'React HITS',
  public: true,
  description: 'React hits'
}, {
  id: '213',
  name: 'React Top20',
  public: false,
  description: 'top20 hits'
}, {
  id: '234',
  name: 'React Best of',
  public: true,
  description: 'Best of reactt hits'
}];
