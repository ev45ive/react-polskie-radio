import { act, render, screen } from '@testing-library/react'
import React from 'react'
import { PlaylistsView } from './PlaylistsView'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistList } from '../components/PlaylistList'
import { playlistsMock } from './playlistsMock'
import { Playlist } from '../../core/models/Playlist'


jest.mock('../components/PlaylistDetails')
jest.mock('../components/PlaylistList')

// jest.mock('./playlistsMock')
jest.mock('./playlistsMock', () => ({
  playlistsMock: [
    { id: 123, name: 'Placki 123' },
    { id: 234, name: 'Placki 234' },
  ]
}))

describe('PlaylistsView', () => {
  // let rerender: any
  // let dataMock: jest.Mock<Playlist[]>
  let mockedListCmp: jest.Mock<any>
  let mockedDetailsCmp: jest.Mock<any>

  beforeEach(() => {
    mockedListCmp = (PlaylistList as jest.Mock<any>)
    mockedDetailsCmp = (PlaylistDetails as jest.Mock<any>)
    mockedListCmp.mockReturnValue('PlaylistListMOCK');
    mockedDetailsCmp.mockReturnValue('PlaylistDetailsMOCK')

    // dataMock = playlistsMock as unknown as jest.Mock<Playlist[]>

    const { rerender: _rerender } = render(<PlaylistsView />)
    // rerender = _rerender
  })


  it('Renders list of playlists', () => {
    const mockedListCmp = (PlaylistList as unknown as jest.Mock<typeof PlaylistList>);

    expect(mockedListCmp).toHaveBeenCalledWith(expect.objectContaining({
      playlists: [
        { id: 123, name: 'Placki 123' },
        { id: 234, name: 'Placki 234' },
      ],
    }), expect.anything())
  })

  it('should select playlist and denders defails of selected playlist', () => {
    const { onSelectId } = (mockedListCmp.mock.calls[0][0]);

    // Warning: An update to PlaylistsView inside a test was not wrapped 
    // in act(...).
    act(() => {
      onSelectId(234)
    })

    expect((PlaylistDetails as jest.Mock)).toHaveBeenLastCalledWith(expect.objectContaining({
      playlist: { id: 234, name: 'Placki 234' }
    }), {});

    screen.debug()
  })

  it.todo('Renders edit form for selected playlist')
})
