import { Playlist } from "../../../core/models/Playlist";


export const playlistsMock: Partial<Playlist>[] = [{
  id: '123',
  name: 'MOCK React HITS',
  public: true,
  description: 'React hits'
}, {
  id: '213',
  name: 'MOCK React Top20',
  public: false,
  description: 'top20 hits'
}, {
  id: '234',
  name: 'MOCK React Best of',
  public: true,
  description: 'Best of reactt hits'
}];
