// tsrafc 
import React, { useCallback, useEffect, useState } from 'react'
import { Playlist, PlaylistEditDraft } from '../../core/models/Playlist'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistEditForm } from '../components/PlaylistEditForm'
import { PlaylistList } from '../components/PlaylistList'
import { playlistsMock } from './playlistsMock'

interface Props { }

export const PlaylistsView = (props: Props) => {
  const [mode, setMode] = useState<'details' | 'edit' | 'review'>('details')
  const [selectedId, setSelectedId] = useState<Playlist['id']>()
  const [selected, setSelected] = useState<Playlist>()
  const [playlists, setPlaylists] = useState(playlistsMock as Playlist[])
  const [filterQuery, setFilterQuery] = useState('')
  const [filteredPlaylists, setFilteredPlaylists] = useState(playlistsMock as Playlist[])

  useEffect(() => {
    setFilteredPlaylists(playlists.filter(p => p.name.toLocaleLowerCase().includes(filterQuery.toLocaleLowerCase())))
  }, [playlists, filterQuery])

  useEffect(() => {
    const selected: Playlist | undefined = playlists.find(//
      p => p.id === selectedId)
    setSelected(selected)
    setMode('details')
  }, [selectedId, playlists])

  const switchToEdit = useCallback(() => { setMode('edit') }, [])
  const switchToDetails = useCallback(() => { setMode('details') }, [])
  const saveChanges = useCallback((draft: PlaylistEditDraft) => {
    setPlaylists(playlists.map(p => p.id === draft.id ? {
      ...p,
      ...draft
    } : p))
    setMode('details')
  }, [playlists])

  return (
    <div>
      <div className="row">
        <div className="col">
          <input type="text" className="form-control" onChange={e => setFilterQuery(e.target.value)} />
          <PlaylistList
            playlists={filteredPlaylists}
            onSelectId={setSelectedId}
            selectedId={selectedId} />
        </div>
        <div className="col">
          {selected && <>
            {mode === 'details' && <PlaylistDetails playlist={selected} onEdit={switchToEdit} />}
            {mode === 'edit' && <PlaylistEditForm playlist={selected}
              onCancel={switchToDetails}
              onSave={draft => saveChanges(draft)} />}
          </>}

          {!selected && <p>Please select playlist</p>}
        </div>
      </div>

    </div>
  )
}
