import React, { FC } from 'react'
import { Playlist } from '../../core/models/Playlist'


interface Props {
  playlist: Playlist,
  onEdit(): void
}

export const PlaylistDetails: FC<Props> = (({ playlist, onEdit }) => {

  // console.log('render details')
  return (
    <div>
      <h4>Details</h4>
      <dl>
        <dt>Name:</dt>
        <dd data-testid="playlist_name">{playlist.name}</dd>
        <dt>Public:</dt>
        <dd data-testid="playlist_public" style={{ color: playlist?.public ? 'red' : 'green' }}
        >{playlist.public ? 'Yes' : 'No'}</dd>
        <dt>Description:</dt>
        <dd data-testid="playlist_description">{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>Edit</button>
    </div>
  )
}/* ,propsAreEqual?: ((prevProps: Readonly<React.PropsWithChildren<Props>>, nextProps: Readonly<React.PropsWithChildren<Props>>) => boolean) | undefined): */)

// PlaylistDetails.defaultProps = {
//   playlist: {
//     id: 213,
//     name: 'React HITS',
//     public: true,
//     description: 'Best hits'
//   }
// }