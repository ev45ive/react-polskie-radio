import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { PlaylistDetails } from './PlaylistDetails';
import renderer from 'react-test-renderer';
import { Playlist } from '../../core/models/Playlist';

describe('PlaylistDetails', () => {
  let playlist: Playlist;
  let onEditSpy: jest.Mock<any, any>;

  it('shoud test', () => { expect(true).toBeTruthy() })

  beforeEach(() => {
    onEditSpy = jest.fn()
    playlist = {
      id: 123,
      name: 'React HITS',
      public: true,
      description: 'React hits'
    }
    render(<PlaylistDetails playlist={playlist} onEdit={onEditSpy} />)
  })

  it('renders playlist details', () => {
    expect(screen.getByTestId('playlist_name')).toHaveTextContent('React HITS')
    expect(screen.getByTestId('playlist_public')).toHaveTextContent('Yes')
    expect(screen.getByTestId('playlist_description')).toHaveTextContent('React hits')
  })

  it('renders playlist details -- snapshot', () => {
    const tree = renderer.create(<PlaylistDetails playlist={playlist} onEdit={onEditSpy} />)
    expect(tree).toMatchSnapshot()
  })

  it('should change "public" field color', () => {
    expect(screen.getByTestId('playlist_public')).toHaveStyle({ color: 'red' })
  })

  it(' "edit" button notifies parent', () => {
    const editButton = screen.getByText('Edit');
    fireEvent.click(editButton)
    expect(onEditSpy).toHaveBeenCalled()
  })

})