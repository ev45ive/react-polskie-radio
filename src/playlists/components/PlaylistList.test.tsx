import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import { PlaylistList } from './PlaylistList'
import { Playlist } from '../../core/models/Playlist'

describe('PlaylistList', () => {

  it('renders empty list', () => {
    render(<PlaylistList playlists={[]} selectedId={undefined} onSelectId={() => { }} />)
    // expect(screen.queryByTestId('playlist-item')).toBeNull()
    // expect(screen.queryByTestId('playlist-item')).not.toContainElement(...)
    expect(screen.queryByTestId('playlist-item')).not.toBeInTheDocument()
  })

  describe(' with data ', () => {
    let playlistsMock: Playlist[]
    let selectSpy = jest.fn()
    let rerender: any

    beforeEach(() => {
      playlistsMock = [{
        id: '123',
        name: 'React HITS',
        public: true,
        description: 'React hits'
      }, {
        id: '213',
        name: 'React Top20',
        public: false,
        description: 'top20 hits'
      }, {
        id: '234',
        name: 'React Best of',
        public: true,
        description: 'Best of reactt hits'
      }] as Playlist[];
      const view = render(<PlaylistList playlists={playlistsMock} selectedId={'123'} onSelectId={selectSpy} />)
      rerender = view.rerender
    })

    it('renders playlists list', () => {
      expect(screen.queryAllByTestId('playlist-item')).toHaveLength(3)
      expect(screen.queryAllByTestId('playlist-item')[0]).toHaveTextContent('React HITS')
    })

    it('highlights one selected playlist', () => {
      expect(screen.getByText(/React HITS/)).toHaveClass('active')
      expect(screen.getByText('2. React Top20')).not.toHaveClass('active')
    })

    it('notifies when selection changes', () => {
      const item = (screen.getByText('2. React Top20'))
      fireEvent.click(item)
      expect(selectSpy).toHaveBeenCalledWith(213)
    })

    it('updates active class when selection changes', () => {
      // cleanup()
      rerender(<PlaylistList playlists={playlistsMock} selectedId={'123'} onSelectId={selectSpy} />)

      expect(screen.getByText(/React HITS/)).toHaveClass('active')
      expect(screen.getByText('2. React Top20')).not.toHaveClass('active')

      rerender(<PlaylistList playlists={playlistsMock} selectedId={'213'} onSelectId={selectSpy} />)

      expect(screen.getByText(/React HITS/)).not.toHaveClass('active')
      expect(screen.getByText('2. React Top20')).toHaveClass('active')
    })
  })


})