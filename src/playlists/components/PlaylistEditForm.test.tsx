import { render, screen, fireEvent } from '@testing-library/react'
import React from 'react'
import { PlaylistEditForm } from './PlaylistEditForm'
import userEvent from '@testing-library/user-event'
import { Playlist, PlaylistEditDraft } from '../../core/models/Playlist'

describe('PlaylistEditForm', () => {
  let cancelSpy: jest.Mock<void, []>
  let saveSpy: jest.Mock<void, [PlaylistEditDraft]>
  let rerender: Function

  beforeEach(() => {
    cancelSpy = jest.fn()
    saveSpy = jest.fn()
    const playlist = {
      id: '123',
      name: 'React HITS',
      public: true,
      description: 'React hits'
    } as Playlist
    const { rerender: _rerender } = render(
      <PlaylistEditForm playlist={playlist} onCancel={cancelSpy} onSave={saveSpy} />
    )
    // console.log(getRoles(container))
    rerender = _rerender
  })

  let confirmSpy: jest.SpyInstance<boolean, [message?: string | undefined]>
  beforeEach(() => {
    confirmSpy = jest.spyOn(window, 'confirm')
  })
  afterEach(() => { confirmSpy.mockRestore() })


  it('renders form with playlist data', () => {
    expect(screen.getByLabelText('Name:')).toHaveValue('React HITS')
    expect(screen.getByLabelText('Public')).toBeChecked()
    expect(screen.getByLabelText('Description:')).toHaveValue('React hits')
  })

  it('allows user to edit playlists data', () => {
    const nameField = (screen.getByLabelText('Name:'))

    expect(nameField).not.toHaveValue('test')
    // fireEvent.input(nameField, { target: { value: 'test' } })
    userEvent.clear(nameField)
    userEvent.type(nameField, 'test')
    expect(nameField).toHaveValue('test')

    const PublicField = (screen.getByLabelText('Public'))
    expect(PublicField).toBeChecked()
    fireEvent.click(PublicField)
    expect(PublicField).not.toBeChecked()

    const DescriptionField = (screen.getByLabelText('Description:'))
    // fireEvent.input(DescriptionField, { target: { value: 'test123' } })
    userEvent.clear(DescriptionField)
    userEvent.type(DescriptionField, 'test123')
    expect(DescriptionField).toHaveValue('test123')
  })

  it('shows available character count', () => {
    const nameField = (screen.getByLabelText('Name:'))
    const counter = (screen.getByTestId('counter'))
    expect(counter).toHaveTextContent('160 / 170')

    userEvent.clear(nameField)
    expect(counter).toHaveTextContent('170 / 170')

    userEvent.type(nameField, '12345')
    expect(counter).toHaveTextContent('165 / 170')
  })

  it('notifies parent when cancel is clicked', () => {
    const cancelBtn = screen.getByRole('button', { name: /Cancel/ })
    confirmSpy.mockImplementationOnce(() => true)
    userEvent.click(cancelBtn)
    expect(cancelSpy).toBeCalled()
  })

  it('needs unsaved change confirmation before exit', () => {
    const cancelBtn = screen.getByRole('button', { name: /Cancel/ })
    // confirmSpy.mockReset()
    confirmSpy.mockImplementationOnce(() => false)
    userEvent.click(cancelBtn)
    expect(cancelSpy).not.toHaveBeenCalled()

    confirmSpy.mockImplementationOnce(() => true)
    userEvent.click(cancelBtn)
    expect(cancelSpy).toHaveBeenCalled()
  })

  it('sends updated draft to parent when save is clicked', () => {
    // Arrange / Given
    const SaveBtn = screen.getByRole('button', { name: /Save/ })
    // Act / When
    userEvent.click(SaveBtn)
    // Assert / Then
    // saveSpy.mock.calls[0][0] 
    expect(saveSpy).toHaveBeenCalledWith({
      "id": 123,
      "description": "React hits",
      "name": "React HITS",
      "public": true
    })
    const nameField = (screen.getByLabelText('Name:'))
    userEvent.clear(nameField)
    userEvent.type(nameField, 'test')

    const publicField = (screen.getByLabelText('Public'))
    userEvent.click(publicField)

    const DescriptionField = (screen.getByLabelText('Description:'))
    userEvent.clear(DescriptionField)
    userEvent.type(DescriptionField, 'test')

    userEvent.click(SaveBtn)

    expect(saveSpy).toHaveBeenLastCalledWith({
      "name": "test",
      "id": 123,
      "description": "test",
      "public": false
    })
  })

  it('should confirm when diffrent playlist is selected and update', () => {



    const playlist = {
      id: 234,
      name: 'Changed',
      public: true,
      description: 'React hits'
    }
    confirmSpy.mockReturnValueOnce(false)
    rerender(<PlaylistEditForm playlist={playlist} onCancel={cancelSpy} onSave={saveSpy} />)
    expect(confirmSpy).toHaveBeenCalled()
    expect(screen.getByLabelText('Name:')).not.toHaveValue('Changed')

    const playlist2 = {
      id: 234,
      name: 'Changed2',
      public: true,
      description: 'React hits'
    }
    // confirmSpy.mockImplementationOnce(() => true)    
    confirmSpy.mockReturnValueOnce(true)
    rerender(<PlaylistEditForm playlist={playlist2} onCancel={cancelSpy} onSave={saveSpy} />)
    expect(confirmSpy).toHaveBeenCalled()
    expect(screen.getByLabelText('Name:')).toHaveValue('Changed2')

  })


})