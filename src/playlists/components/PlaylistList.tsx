import React from 'react'
import { Playlist } from '../../core/models/Playlist'

interface Props {
  playlists: Playlist[],
  selectedId?: Playlist['id']
  onSelectId(id: Playlist['id']): void
}

export const PlaylistList = ({ 
  playlists, 
  onSelectId, 
  selectedId 
}: Props) => {

  return (
    <div>
      {/* .list-group>.list-group-item*3{Playlist Name} */}
      <div className="list-group">
        {playlists.map((playlist, index) =>
          <div className={`list-group-item ${playlist.id === selectedId ? 'active' : ''}`} key={playlist.id} onClick={() => onSelectId(playlist.id)} data-testid="playlist-item">
            {index + 1}. {playlist.name}
          </div>
        )}
      </div>
    </div>
  )
}
