import React, { useEffect, useRef, useState } from 'react'
import { Playlist, PlaylistEditDraft } from '../../core/models/Playlist'

interface Props {
  playlist: Playlist,
  onCancel(): void,
  onSave(draft: PlaylistEditDraft): void
}

export const PlaylistEditForm = ({ playlist, onCancel, onSave }: Props) => {
  const [name, setName] = useState(playlist.name)
  const [isPublic, setIsPublic] = useState(playlist.public)
  const [description, setDescription] = useState(playlist.description)
  const nameInputRef = useRef<HTMLInputElement>(null)
  const isFirst = useRef(true)

  useEffect(() => {
    if (isFirst.current) { isFirst.current = false; return }
    if (!window.confirm('Will reset form. Are you sure')) { return }
    setName(playlist.name)
    setIsPublic(playlist.public)
    setDescription(playlist.description)
  }, [playlist])

  useEffect(() => {
    nameInputRef.current?.focus()
  }, [])

  const cancel = () => {
    const discardChanges = window.confirm('Will discard changes. Are you sure?')
    if (discardChanges) {
      onCancel()
    }
  }

  // console.log('rerender')
  return (
    <div>
      {/* <pre>{JSON.stringify({ name, isPublic, description }, null, 2)}</pre> */}

      <div className="form-group">
        <label htmlFor="playlist_name">Name:</label>
        <input id="playlist_name" type="text" className="form-control" value={name} onChange={e => setName(e.target.value)} ref={nameInputRef} />
        <p data-testid="counter">{170 - name.length} / 170</p>
      </div>

      <div className="form-group">
        <label><input type="checkbox" checked={isPublic} onChange={e => setIsPublic(e.target.checked)} /> Public</label>
      </div>

      <div className="form-group">
        <label htmlFor="playlist_description">Description:</label>
        <textarea id="playlist_description" className="form-control" value={description} onChange={e => setDescription(e.target.value)}></textarea>
      </div>

      <button className="btn btn-danger" onClick={cancel}>Cancel</button>
      <button className="btn btn-success" onClick={
        () => onSave({
          id: playlist.id,
          name,
          public: isPublic,
          description
        })
      }>Save</button>
    </div>
  )
}
