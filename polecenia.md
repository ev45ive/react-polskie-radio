# GIT
git clone https://bitbucket.org/ev45ive/react-polskie-radio.git
cd react-polskie-radio
npm install
npm start

# Instalacje 
node -v 
v14.5.0

npm -v 
6.14.5

git --version
chrome

npm install -g create-react-app
create-react-app --version
4.0.0
create-react-app --help

# Generate typescript react project
cd ..
create-react-app react-polskie-radio --template=typescript

# bootstrap
npm install bootstrap
yarn add bootstrap

# Aria
https://www.w3.org/TR/html-aria/#document-conformance-requirements-for-use-of-aria-attributes-in-html
https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-labelledby_attribute
https://testing-library.com/docs/dom-testing-library/api-helpers#getroles

# Testing Library
https://testing-library.com/docs/dom-testing-library/api-queries
https://github.com/testing-library/jest-dom
https://testing-library.com/docs/dom-testing-library/api-events
https://github.com/testing-library/user-event


# Mocking implementation
https://jestjs.io/docs/en/es6-class-mocks#replacing-the-mock-using-mockimplementationdocsenmock-function-apimockfnmockimplementationfn-or-mockimplementationoncedocsenmock-function-apimockfnmockimplementationoncefn

# Coverage 
npm test -- --coverage
https://create-react-app.dev/docs/running-tests/#coverage-reporting

# Debug tests
https://create-react-app.dev/docs/debugging-tests
npm test:debug

# TypeScript model from JSON
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype


# Redux
yarn add redux react-redux @types/react-redux

