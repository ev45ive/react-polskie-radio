```js
[1,2,3,4,5].reduce((sum,n)=>{
    console.log(sum,n, sum+n)
    return sum + n;
},0) 


events = [1,2,3,4,5]

events.reduce((state,n)=>{
    return {...state, counter: state.counter + n }
},{
    todos:[],
    counter:0
}) 

```
/////////////
```js
inc = s => s + 1
dec = s => s - 1

events = [inc,dec,inc,inc]

events.reduce((state,fn)=>{
    return {...state, counter: fn(state.counter) }
},{
    todos:[],
    counter:0
}) 
```
///////////`//
```js
inc = s => s + 1
dec = s => s - 1
addTodo = s => [...s,'new todo'] 

events = [inc,dec,inc,inc, addTodo]

events.reduce((state,fn)=>{
    return {...state, counter: fn(state.counter) }
//     return fn(state)
},{
    todos:[],
    counter:0
}) 
```
/////////////////
```js
inc = {type:'INC', payload:1};
dec = {type:'DEC', payload:1};
addTodo = {type:'ADD_TODO', payload:{name:'new todo'}};

events = [inc,dec,inc,inc, addTodo];

events.reduce((state,action)=>{
    switch(action.type){
        case 'INC': return {...state, counter: state.counter + action.payload };
        case 'DEC': return {...state, counter: state.counter - action.payload }; 
        case 'ADD_TODO': return {...state, todos: [...state.todos, action.payload ] }; 
        default: return state;
    }
},{
    todos:[],
    counter:0
}) 
{todos: Array(1), counter: 2}
counter: 2
todos: Array(1)
0: {name: "new todo"}

```
/////////////////
```js

inc = {type:'INC', payload:1};
dec = {type:'DEC', payload:1};
addTodo = {type:'ADD_TODO', payload:{name:'new todo'}};

function reducer(state,action){
    switch(action.type){
        case 'INC': return {...state, counter: state.counter + action.payload };
        case 'DEC': return {...state, counter: state.counter - action.payload }; 
        case 'ADD_TODO': return {...state, todos: [...state.todos, action.payload ] }; 
        default: return state;
    }
}
state = {
    todos:[],
    counter:0
}

console.log( state = reducer(state, inc))
console.log( state = reducer(state, inc))
console.log( state = reducer(state, dec))
console.log( state = reducer(state, inc))
console.log( state = reducer(state, addTodo))


```
/////////////////
```js

inc = {type:'INC', payload:1};
dec = {type:'DEC', payload:1};
addTodo = {type:'ADD_TODO', payload:{name:'new todo'}};

function counterReducer(state = 0,action){
    switch(action.type){
        case 'INC': return state + action.payload;
        case 'DEC': return state - action.payload;
        default: return state; 
    }
}

function reducer(state,action){
    switch(action.type){        
        case 'ADD_TODO': return {...state, todos: [...state.todos, action.payload ] }; 
        default: return {
            ...state,
            counter: counterReducer(state.counter, action)
        };
    }
}
state = {
    todos:[],
    counter:0
}

console.log( state = reducer(state, inc))
console.log( state = reducer(state, inc))
console.log( state = reducer(state, dec))
console.log( state = reducer(state, inc))
console.log( state = reducer(state, addTodo))

```